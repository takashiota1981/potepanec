class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.products
    @categories = Spree::Taxon.find_by(name: 'Categories').leaves
    @brands = Spree::Taxon.find_by(name: 'Brand').leaves
  end
end
